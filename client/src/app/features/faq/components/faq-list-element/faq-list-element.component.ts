import { Component } from '@angular/core';

@Component({
  selector: 'shopify-faq-list-element',
  templateUrl: './faq-list-element.component.html',
  styleUrls: ['./faq-list-element.component.scss'],
})
export class FaqListElementComponent {}
