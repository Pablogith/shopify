import { Component } from '@angular/core';

@Component({
  selector: 'shopify-faq-list',
  templateUrl: './faq-list.component.html',
  styleUrls: ['./faq-list.component.scss'],
})
export class FaqListComponent {}
