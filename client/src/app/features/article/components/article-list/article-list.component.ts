import { Component } from '@angular/core';

@Component({
  selector: 'shopify-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
})
export class ArticleListComponent {}
