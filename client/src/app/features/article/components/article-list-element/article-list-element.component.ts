import { Component } from '@angular/core';

@Component({
  selector: 'shopify-article-list-element',
  templateUrl: './article-list-element.component.html',
  styleUrls: ['./article-list-element.component.scss'],
})
export class ArticleListElementComponent {}
