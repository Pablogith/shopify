import { Component } from '@angular/core';

@Component({
  selector: 'shopify-layout-user-navigation',
  templateUrl: './layout-user-navigation.component.html',
  styleUrls: ['./layout-user-navigation.component.scss'],
})
export class LayoutUserNavigationComponent {}
