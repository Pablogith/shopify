import { Component } from '@angular/core';

@Component({
  selector: 'shopify-order-card-list',
  templateUrl: './order-card-list.component.html',
  styleUrls: ['./order-card-list.component.scss'],
})
export class OrderCardListComponent {}
