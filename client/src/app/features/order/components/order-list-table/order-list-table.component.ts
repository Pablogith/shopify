import { Component } from '@angular/core';

@Component({
  selector: 'shopify-order-list-table',
  templateUrl: './order-list-table.component.html',
  styleUrls: ['./order-list-table.component.scss'],
})
export class OrderListTableComponent {}
