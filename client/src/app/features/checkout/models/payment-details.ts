export interface PaymentDetails {
  cardNumber: number;
  expirationDate: string;
  cvv: number;
}
