export interface ShippingAddress {
  address: string;
  city: string;
  postalCode: string;
  flatNumber: string;
}
