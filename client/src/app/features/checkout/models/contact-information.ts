export interface ContactInformation {
  email: string;
  phoneNumber: string;
}
