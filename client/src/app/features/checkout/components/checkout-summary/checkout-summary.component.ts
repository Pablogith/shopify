import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shopify-checkout-summary',
  templateUrl: './checkout-summary.component.html',
  styleUrls: ['./checkout-summary.component.scss'],
})
export class CheckoutSummaryComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
