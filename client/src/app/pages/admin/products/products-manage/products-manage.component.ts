import { Component } from '@angular/core';

@Component({
  selector: 'shopify-products-manage',
  templateUrl: './products-manage.component.html',
  styleUrls: ['./products-manage.component.scss'],
})
export class ProductsManageComponent {}
