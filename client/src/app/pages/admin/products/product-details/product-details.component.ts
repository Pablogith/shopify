import { Component } from '@angular/core';

@Component({
  selector: 'shopify-product-details-page',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent {}
