import { Component } from '@angular/core';

@Component({
  selector: 'shopify-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {}
