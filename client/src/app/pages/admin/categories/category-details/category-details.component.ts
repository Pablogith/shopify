import { Component } from '@angular/core';

@Component({
  selector: 'shopify-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.scss'],
})
export class CategoryDetailsComponent {}
