import { Component } from '@angular/core';

@Component({
  selector: 'shopify-categories-manage',
  templateUrl: './categories-manage.component.html',
  styleUrls: ['./categories-manage.component.scss'],
})
export class CategoriesManageComponent {}
