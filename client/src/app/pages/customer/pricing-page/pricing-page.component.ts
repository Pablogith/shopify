import { Component } from '@angular/core';

@Component({
	selector: 'shopify-pricing-page',
	templateUrl: './pricing-page.component.html',
	styleUrls: ['./pricing-page.component.scss'],
})
export class PricingPageComponent {}
