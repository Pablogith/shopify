import { Component } from '@angular/core';

@Component({
  selector: 'shopify-not-found-page',
  templateUrl: './not-found-page.component.html',
  styles: [],
})
export class NotFoundPageComponent {}
