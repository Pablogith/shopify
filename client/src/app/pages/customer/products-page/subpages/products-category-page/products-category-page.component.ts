import { Component } from '@angular/core';

@Component({
  selector: 'shopify-products-category-page',
  templateUrl: './products-category-page.component.html',
  styleUrls: ['./products-category-page.component.scss'],
})
export class ProductsCategoryPageComponent {}
