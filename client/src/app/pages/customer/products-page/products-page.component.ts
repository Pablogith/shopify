import { Component } from '@angular/core';

@Component({
  selector: 'shopify-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss'],
})
export class ProductsPageComponent {}
