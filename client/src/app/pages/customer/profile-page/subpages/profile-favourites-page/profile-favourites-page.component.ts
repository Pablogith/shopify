import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shopify-profile-favourites-page',
  templateUrl: './profile-favourites-page.component.html',
  styleUrls: ['./profile-favourites-page.component.scss'],
})
export class ProfileFavouritesPageComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
