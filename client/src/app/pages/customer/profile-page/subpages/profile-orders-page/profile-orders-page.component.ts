import { Component } from '@angular/core';

@Component({
  selector: 'shopify-profile-orders-page',
  templateUrl: './profile-orders-page.component.html',
  styleUrls: ['./profile-orders-page.component.scss'],
})
export class ProfileOrdersPageComponent {}
