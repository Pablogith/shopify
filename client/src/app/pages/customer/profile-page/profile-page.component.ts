import { Component } from '@angular/core';

@Component({
  selector: 'shopify-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent {}
