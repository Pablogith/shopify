import { Category } from '../category.entity';

export interface FindAllCategoriesDto {
  categories: Category[];
}
