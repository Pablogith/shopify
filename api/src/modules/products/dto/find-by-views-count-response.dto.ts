export interface FindByViewsCountResponseDto {
  productViews: number | string;
  productId: number;
  productName: string;
  productImage: string;
  categoryId: number;
}
