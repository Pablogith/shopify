export interface FindAllUsersDto {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  createdAt: string;
}
